---
title: Web
layout: default
has_children: true
nav_order: 3
---
# Web mode

## Enabling

The web mode is enabled by default. To disable it, simply append `--no-web` to the parameters
