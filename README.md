# Friendly Telegram Docs

This repository is where the docs of FTG lives.

## Local Development

Make sure your machine has latest Ruby and Bundle installed. If you prefer not to mess your machine
with dependencies, [open this repository in Gitpod][gitpod].

[gitpod]: https://gitpod.io/#https://gitlab.com/friendly-telegram/friendly-telegram.gitlab.io

1. Clone the Git repository with `git clone https://gitlab.com/friendly-telegram/friendly-telegram.gitlab.io ftg-docs`.
2. Navigate to it with `cd ftg-docs` and install dependencies with `bundle install`.
3. Launch an new development server with `bundle exec jekyll serve`. Preview your work on `localhost:4000`.
4. Edit as usual. If changes are applied and the server doesn't reload your browser, press <button>F5</button>.
5. When you're happy, add your changed files to the index and signoff your commits.
6. Run `git push` and wait for the GitLab CI to roll on.
