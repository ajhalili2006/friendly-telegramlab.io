---
title: Friendly Telegram
layout: default
nav_exclude: true
---

# {{ page.title }}

Welcome to the Friendly-Telegram documentation! This website is where the documentation of Friendly Telegram
lives, and this was made with Jekyll, built with GitLab CI, and hosted on GitLab Pages.
